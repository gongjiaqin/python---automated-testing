from utils.chrome import ChromeWrapper


class BasePage:
    def __init__(self, url, chrome: ChromeWrapper):
        self.url = url
        self.chrome = chrome

    def open(self):
        self.chrome.open(self.url)