import os

# 获取当前脚本的绝对路径  abspath()
# __file__ 当前脚本文件路径
# 获取当前的脚本的项目目录
# dirname() 获取路径的目录位置
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# .join(dir, filename) 将文件名和dir目录拼接在一起
DATA_DIR = os.path.join(BASE_DIR, 'test_datas')

RESULT_DIR = os.path.join(BASE_DIR, 'test_results')

RESULT_IMG_DIR = os.path.join(RESULT_DIR, 'images')