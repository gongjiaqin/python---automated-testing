import time

from selenium.webdriver import Chrome, ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec

class NoElementError(BaseException):
    pass

class ChromeWrapper(Chrome):
    def __init__(self):
        super(ChromeWrapper, self).__init__()
        self.sleep = 1

    def open(self, url):
        self.get(url)
        time.sleep(self.sleep)

    def __find_one(self, selector) -> WebElement:
        '''
        定位元素
        :param selector: #id, .class, tag_name,tag_name[属性=属性值], //tagname, $link_text
        :return: WebElement
        '''
        if selector.startswith('/'):
            return self.find_element_by_xpath(selector)
        elif selector.startswith('$'):
            return self.find_element_by_link_text(selector[1:])
        else:
            return self.find_element_by_css_selector(selector)

    def input(self, selector, value):
        time.sleep(self.sleep)
        try:
            element = self.__find_one(selector)
            element.clear() # 清空原数据
            element.send_keys(value)
        except:
            raise NoElementError(f'未发现{selector} 元素')

    def click(self, selector):
        time.sleep(self.sleep)
        try:
            self.__find_one(selector).click()
        except:
            raise NoElementError(f'未发现{selector} 元素')

    def dbclick(self, selector):
        time.sleep(self.sleep)
        try:
            ActionChains(self).move_to_element(self.__find_one(selector)).double_click().perform()
        except:
            raise NoElementError(f'未发现{selector} 元素')

    def save_png(self, path):
        self.save_screenshot(path)

    def save_cookie(self, path):
        # 获取cookies，并存储
        cookies = self.get_cookies()

        with open(path, 'w', encoding='utf-8') as f:
            for cookie_dict in cookies:
                # dict对象可以直接写入文件吗？
                # 将dict转化为字符串，再写入文件中
                f.write(str(cookie_dict) + "\n")

    def load_cookie(self, path):
        with open(path, 'r', encoding='utf-8') as f:
            for line in f:
                cookie_dict = eval(line)
                self.add_cookie(cookie_dict)

    def frame(self, selector, default=True):
        if default:
            self.switch_to.default_content()
        time.sleep(self.sleep)

        try:
            self.switch_to.frame(self.__find_one(selector))
        except:
            raise NoElementError(f'{selector} 指定的frame不存在')

    def window(self, index):
        self.switch_to.window(self.window_handles[index])
        time.sleep(self.sleep)

    @property
    def by(self):
        return By

    def wait_ui(self, selector: tuple, timeout=5):
        # 阻塞的方法，等待某一个元素出现
        WebDriverWait(self, timeout).until(
            ec.visibility_of_element_located(selector),
            f'{selector} 元素未出现'
        )

        # 以上方法执行时不报异常，则表示元素可见或可定位