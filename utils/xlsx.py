"""
读和写excel文件数据
"""
import openpyxl
import os

from common import DATA_DIR, RESULT_DIR

def _data_full_path(path):
    return os.path.join(DATA_DIR, path)

def read_data(filename, has_title=True, sheet_index=0):
    '''
    读取 test_datas/{path} excel的文件数据
    :param path:
    :return:
    '''
    path = _data_full_path(filename)
    wb = openpyxl.load_workbook(path) # path是完整的文件路径
    sheet1 = wb[wb.sheetnames[sheet_index]]
    # 获取行数和列数
    # .rows 返回是单元格的名称的生成器
    rows = sheet1.rows
    if has_title:
        # next(rows) 返回一行数据，一行数据包含多个Cell
        titles = [cell.value for cell in next(rows) ]

    datas = []
    for columns in rows:
        # 每一行存在多个数据单元Cell类实例（元组）
        data = [cell.value if cell.value is not None else '' for cell in columns ]
        datas.append(data)

    return datas, (titles if has_title else None)


def write_data(filename, datas):
    # 测试失败情况下，写入到excel文件中
    path = os.path.join(RESULT_DIR, filename)

    # datas表示一条数据（多列）
    if not os.path.exists(path):
        wb = openpyxl.Workbook()
        sheet = wb.active
        sheet.title = '测试结果'
    else:
        # 已存在
        wb = openpyxl.load_workbook(path)
        sheet = wb.active

    sheet.append(datas)
    wb.save(path)


if __name__ == '__main__':
    # datas, titles = read_data('regist_data.xlsx')
    # print(datas)
    write_data('a.xlsx', ['disen', '666', '999', 1])