import unittest

from BeautifulReport import BeautifulReport

from day04.test_oa_login_ddt import TestOaWebLogin


suite=unittest.TestSuite()
suite.addTests(unittest.TestLoader().loadTestsFromTestCase(TestOaWebLogin))


bf = BeautifulReport(suite)
bf.report('OA登录测试', 'report4.html')

