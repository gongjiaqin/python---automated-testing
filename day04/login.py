# -*- coding: utf-8 -*-
"""
@Author: Allen
@Date 2021/3/17 8:39
@FileName:login.py
"""

def login_check(username=None,password=None):
    """
    登录校验的函数
    :param username: 账号
    :param password: 密码
    :return: dict type
    """
    if username != None and password != None:
        if username == 'qftest01' and password == 'qftest01.':
            return {"code":0,"msg":"登录成功"}
        else:
            return {"code":1,"msg":"账号或密码不正确"}
    else:
        return {"code":1,"msg":"参数不能为空"}