import  unittest
from day04 import login #白盒测试模块
class TestLogin(unittest.TestCase):
    #前置后置方法
    @classmethod
    def setUpClass(cls) -> None:
        pass
    @classmethod
    def tearDownClass(cls) -> None:
        pass
    def setUp(self) -> None:
        pass
    def tearDown(self) -> None:
        pass



    #测试方法->测试用例
    def test_login_ok(self):
        ret=login.login_check('qftest01','qftest01.')
        expect={"code":0,"msg":"登录成功"}
        if ret==expect:
            print('成功')
        else:
            print('失败')
            raise AssertionError(f'期待:{expect},真实:{ret}')  #测试成功
    def test_login2(self):
        ret = login.login_check('qftest01', '')
        expect = {"code":1,"msg":"账号或密码不正确"}
        if ret == expect:
            print('成功')
        else:
            print('失败')
            raise AssertionError(f'期待:{expect},真实:{ret}')  #测试失败
    def test_login3(self):
        ret = login.login_check('qftest01', None)
        expect = {"code":1,"msg":"账号或密码不正确"}
        if ret == expect:
            print('成功')
        else:
            print('失败')
            raise AssertionError(f'期待:{expect},真实:{ret}')  #测试失败

if __name__ == '__main__':
    unittest.main()




















