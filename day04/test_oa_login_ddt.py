import time
import unittest
from unittest import TestCase

from utils.chrome import ChromeWrapper

class TestOaWebLogin(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        # 创建浏览器对象
        cls.chrome = ChromeWrapper()

    @classmethod
    def tearDownClass(cls) -> None:
        cls.chrome.quit()

    def setUp(self) -> None:
        self.chrome.open('http://39.101.167.251/MiFengOA/')
        # time.sleep(1)

    def tearDown(self) -> None:
        self.chrome.delete_all_cookies()

    def test_login_admin(self):
        self.chrome.input('[name="adminuser"]', 'admin')
        self.chrome.input('[class=input][type="password"]', 'a8983983')

        self.chrome.click('[class=webbtn][onclick="loginsubmit()"]')

        # 期待的结果？？  进入主页面（id=indexmenu的div出现）
        self.chrome.wait_ui((self.chrome.by.ID, 'indexmenu'), timeout=2)


    def test_login_rock(self):
        self.chrome.input('[name="adminuser"]', 'rock')
        self.chrome.input('[class=input][type="password"]', 'qf1234567890')

        self.chrome.click('[class=webbtn][onclick="loginsubmit()"]')

        # 期待的结果？？  进入主页面（id=indexmenu的div出现）
        self.chrome.wait_ui((self.chrome.by.ID, 'indexmenu'), timeout=2)

    def test_login_error(self):
        self.chrome.input('[name="adminuser"]', 'rock')
        self.chrome.input('[class=input][type="password"]', '123')

        self.chrome.click('[class=webbtn][onclick="loginsubmit()"]')

        # 期待的结果？？  进入主页面（id=indexmenu的div出现）
        self.chrome.wait_ui((self.chrome.by.CSS_SELECTOR, '#msgview>span'), timeout=2)


if __name__ == '__main__':
    unittest.main()
