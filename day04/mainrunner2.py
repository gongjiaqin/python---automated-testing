import unittest

from day04.test_login_ddt import TestLogin
from utils.BeautifulReport import BeautifulReport

suite=unittest.TestSuite()
suite.addTests(unittest.TestLoader().loadTestsFromTestCase(TestLogin))
bf=BeautifulReport(suite)
bf.report('数据驱动方式','report3.html')
