import unittest
#DDT完全配合unittest的框架
#DDT的装饰器@ddt()：用于引入后续装饰器
#@data()装饰器
#数据格式：列表、字典（@unpack()装饰器解包）、列表嵌套字典
from ddt import ddt,data,unpack

users=[
      {'name':'qftest01','pwd':'qftest01'},
      {'name':'admin','pwd':'admin123'}
    ]

@ddt()
class TestLogin2(unittest.TestCase):
    #@data()中每一条数据即是一个测试用例
    @data('qftest01','qftest01.')
    def test_login1(self,name):
        print(name)

    @data({'name':'qftest01','pwd':'qftest01'},
          {'name':'admin','pwd':'admin123'})
    def test_login2(self, item):
        print(item)

    #@data('disen','jack')
    @data(*['disen','jack'])
    def test_login3(self,item):
        print(item)

    #users->[{},{},{},...]
    @data(*users)
    @unpack  #将每一条数据（字典）中的key和value转化为关键字参数
    def test_login4(self,name,pwd):
        print(name,pwd)


if __name__ == '__main__':
    unittest.main()