import unittest


from ddt import ddt,data,unpack
from day04.login import login_check  #测试目标


datas=[
    {'name':'qftest01','pwd':'qftest01.','expect':{"code":0,"msg":"登录成功"}},
    {'name':'qftest01','pwd':'qftest02.','expect':{"code":0,"msg":"登录成功"}},
    {'name':'disen','pwd':'qftest01.','expect':{"code":1,"msg":"账号或密码不正确"}},
    {'name':'','pwd':'qftest01.','expect':{"code":1,"msg":"账号或密码不正确"}},
    {'name':'','pwd':'','expect':{"code":1,"msg":"账号或密码不正确"}},
    {'name':'','pwd':None,'expect':{"code":1,"msg":"账号或密码不正确"}},
    {'name':'','pwd':None,'expect':{"code":1,"msg":"参数不能为空"}}
]

@ddt()
class TestLogin(unittest.TestCase):

    @data(*datas)
    @unpack
    def test_login(self,name,pwd,expect):
        ret=login_check(name,pwd)
        if ret==expect:
            print(f'{name}/{pwd}测试成功')
        else:
            raise AssertionError(f'期待：{expect}，结果：{ret} 不一致')













