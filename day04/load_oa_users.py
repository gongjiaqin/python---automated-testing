import csv
def load():
    with open('test_users.csv',encoding='utf-8') as f:
        reader=csv.reader(f)
        titles=next(reader)#reader迭代，next()获取迭代器中的元素
        datas=[]
        for data in reader:
            data__dict={}
            data__dict[titles[0]]=data[0]
            data__dict[titles[1]]=data[1]
            data__dict[titles[2]]=data[2].reaplace(';',',')
            datas.append(data__dict)
        return datas
if __name__ == '__main__':
    print(load())
