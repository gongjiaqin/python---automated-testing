import unittest
import day04
#3.导入测试用例所在的类
from day04.test_login import  TestLogin
from day04.d1 import TestUserAuth
from utils.HTMLTestRunner import HTMLTestRunner
from utils.BeautifulReport import BeautifulReport
#第三方安装库的位置：{解释器位置}/lib/site-packages
#1.实例化测试套件
suite=unittest.TestSuite()
#2.实例化测试加载器
loader=unittest.TestLoader()
#4.向套件中添加TestLogin的所有测试方法（用例）
suite.addTest(loader.loadTestsFromTestCase(TestLogin))
suite.addTest(loader.loadTestsFromTestCase(TestUserAuth))
# #5.执行测试用例
#1)控制台打印测试报告
# unittest.main()
'''
不添加套件也可以执行
1）导包
2）执行unittest.main()
'''
#2)创建HTML测试报告
# with open('report1.html','wb')as f:
#     report=HTMLTestRunner(f,title='登录测试报告',description='login')
#     report.run(suite)
bf=BeautifulReport(suite)
bf.report('测试登录用户','report2.html')