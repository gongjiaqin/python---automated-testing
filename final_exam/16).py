import os
'''
16) 设计函数，给定一个文件路径，返回它的文件大小
【提示：文件路径可能是一个目录】
'''
sum = 0
def sum_path_file(dir):
    def sum_file(dir):
        for subpath in os.listdir(dir):
            fullpath=os.path.join(dir,subpath)#关键：拼接完整路径才能确定是不是目录/文件
            if os.path.isdir(fullpath) and len(os.listdir(fullpath))!=0:
                print(subpath, '是目录')
                sum_file(fullpath)
            else:
                print(subpath,'是文件')
                global sum
                sum+=1
    sum_file(dir)
    return sum

if __name__ == '__main__':

    files = sum_path_file(r'd:\webtesting2205\utils')
    print(files)

