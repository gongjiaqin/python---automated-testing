from common.base_page import BasePage
from page_locators import regist_locator


class RegistPage(BasePage):
    def __init__(self, chrome):
        url = 'http://39.101.167.251/qftest/user/register.html'
        super(RegistPage, self).__init__(url, chrome)

    def input_username(self, username):
        self.chrome.input(regist_locator.username, username)

    def input_email(self, email):
        self.chrome.input(regist_locator.email, email)

    def input_password(self, pwd):
        self.chrome.input(regist_locator.password, pwd)

    def input_repassword(self, pwd):
        self.chrome.input(regist_locator.repassword, pwd)

    def regist(self, expect_success=True):
        self.chrome.click(regist_locator.regist_btn)

        # 期待结果
        if expect_success:
            expect = regist_locator.success
            timeout = 8
        else:
            expect = regist_locator.error
            timeout = 2

        by_tuple = self.chrome.by.CSS_SELECTOR, expect
        self.chrome.wait_ui(by_tuple, timeout)