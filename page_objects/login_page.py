from common.base_page import BasePage
from page_locators import login_locator


class LoginPage(BasePage):
    def __init__(self, chrome):
        super().__init__('http://39.101.167.251/qftest/user/login.html', chrome)

    def input_name(self, name):
        self.chrome.input(login_locator.username, name)

    def input_pwd(self, pwd):
        self.chrome.input(login_locator.password, pwd)


    def login(self, expect_type=0):
        """
        :param expect_type:  0  验证非空，  1 账号或口令错误  2 成功
        :return:
        """
        self.chrome.click(login_locator.login_btn)
        if expect_type == 2:
            self.chrome.wait_ui((self.chrome.by.CSS_SELECTOR, login_locator.expect_success))
        elif expect_type == 0:
            self.chrome.wait_ui((self.chrome.by.CSS_SELECTOR, login_locator.expect_error))
        else:
            self.chrome.wait_ui((self.chrome.by.CSS_SELECTOR, login_locator.expect_error_2))
