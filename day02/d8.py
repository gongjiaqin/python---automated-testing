#目标：实现免登录进入主页面，通过cookie
import time
from  selenium.webdriver import Chrome
chrome=Chrome()

#1.先打开oa登录页面
chrome.get('http://39.101.167.251/MiFengOA/')
#2.添加cookie
with open('oa_cookies.txt',encoding='utf-8') as f:
     line=f.readline()
     #print(line)
     #转化{}
     for kv in line.split(';'):
         name,value=kv.strip().split('=',1)
         cookie_dict={'name':name,
                      'value':value,
                      'domain':'39.101.167.251',
                      'path':'/',
                      'ecpires':'2023-09-02T09:17:47.464Z'}
         print(cookie_dict)
         chrome.add_cookie(cookie_dict)
time.sleep(2)
#刷新网页：chrome.refesh()当前地址栏中网址重新加载
chrome.get('http://39.101.167.251/MiFengOA/')