from selenium.webdriver import Chrome,Firefox,Ie,Opera,Safari
from selenium.webdriver.remote.webelement import WebElement
import time
chrome=Chrome()
chrome.get('http://39.101.167.251/MiFengOA/?m=login')
#隐式等待：针对每一个页面加载时长的等待
chrome.implicitly_wait(5)

#1.定位form表单：获取登录的form表单：name=myform
#可以先通过class='lmaisft'的div元素再找form表单
#div:WebElement =chrome.find_element_by_class_name('lmaisft')
#从div元素中获取它的form子标签
#form =div.find_element_by_name('myform')

#2.通过xpath：/div[4],第四个标签：绝对路径/相对路径
#form=chrome.find_element_by_xpath('/html/body/div/div[4]/form')
form=chrome.find_element_by_xpath('//form')

#3.获取记住密码
#rempassd=chrome.find_element_by_xpath('//*[text()="记住密码"]/input')

#1.定位用户名密码输入框
#input_name,input_pwd=form.find_elements_by_tag_name('input')[:2]
#2.通过相对路径加根据xpath精准匹配
input_name,input_pwd=chrome.find_elements_by_xpath('//div[@id="loginview0"]//input')[:2]

input_name.send_keys('admin')
input_pwd.send_keys('a8983983')
#强制等待
time.sleep(2)
chrome.quit()








