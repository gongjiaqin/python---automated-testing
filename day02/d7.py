#目标：slenium+js动态设置元素属性
from selenium.webdriver import Chrome
import  time
chrome=Chrome()
chrome.get('https://www.baidu.com')
#获取新闻链接对象
news=chrome.find_element_by_link_text('新闻')
#修改新闻链接的target属性为_self
#arguments[0]代表网页中的某一个标签
js='arguments[0].setAttribute("target","_self")'
chrome.execute_script(js,news)
#修改新闻文本为“中秋新闻”
js='arguments[0].innertext="中秋新闻"'
chrome.execute_script(js,news)
time.sleep(2)
#点击 新闻链接 进入 新闻页面
news.click()
time.sleep(2)
#模拟人是行为滚动到页面底部
i=0
while i<7200:
    i+=500
    #第一种方式
    # js=f'document.documentElement.scrollTop={i}'
    # chrome.execute_script(js)
    #第二种传参的方式
    js=f'document.documentElement.scrollTop=arguments[0]'#当前页面.当前页面的对象.对象的滚动方法
    chrome.execute_script(js,i)
    time.sleep(1)
chrome.quit()





