from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement

chrome=Chrome()
chrome.get('http://39.101.167.251/MiFengOA/')
#1.隐式等待
chrome.implicitly_wait(3)
#2.先确定父级标签，再确定子标签，一级一级获取：如下，比较麻烦，可以使用xpath方式一次性实现
div= chrome.find_element(By.CLASS_NAME, 'lmaisft')
form = div.find_element_by_name('myform')
input_name, input_pwd = form.find_elements(By.TAG_NAME, 'input')[:2]
# input_name.send_keys('admin')
# input_pwd.send_keys('a8983983')
