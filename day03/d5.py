import time

from selenium.webdriver import ActionChains
from selenium.webdriver.remote.webelement import WebElement
from day03.d2 import chrome
time.sleep(2)

#d2脚本执行完成后，才会执行d3的脚本程序
print('---day03/d3----')
#目标：入库管理-》产品入库-》点击选择已有产品-》在新窗口中windows中双击选择已有产品

left_frame=chrome.find_element_by_name('left')
chrome.switch_to_frame(left_frame)#进入到left窗口中
chrome.find_element_by_xpath('//*[@id="g_1"]//tr[1]').click()
time.sleep(1)
chrome.switch_to.default_content()#切出left
#再进入到right的frame窗口
time.sleep(1)
right_frame=chrome.find_element_by_name('right')
chrome.switch_to.frame(right_frame)
time.sleep(1)
#找到输入框
chrome.find_element_by_name('huohao1').click()
#弹出新窗口
#print(chrome.window_handles)#以列表显示
#切入到第二个窗口
chrome.switch_to_window(chrome.window_handles[1])

#在第二个窗口中，双击第一个产品
sp1=chrome.find_element_by_css_selector('.toptable tr:nth-child(2)')
actions=ActionChains(chrome)
actions.move_to_element(sp1)
#actions.click_and_hold(sp1)  #点击并保持按下鼠标左键
actions.double_click()
actions.perform()

#看切入到第二个windows后，关闭后，切到哪里？
'''
还是在该窗口，显示找不到windows
1）首先回到原来的窗口
2）再定位到right_farme
'''
#通过，输入单价
chrome.switch_to_window(chrome.window_handles[0])
right_frame=chrome.find_element_by_name('right')
chrome.switch_to.frame(right_frame)
time.sleep(1)
price=chrome.find_element_by_name('price21')
price.clear()
price.send_keys('50')










