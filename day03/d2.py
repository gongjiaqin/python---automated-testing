#目标：登录到进销存系统，获取登录之后的所有cookies信息并写入到文件中，以备下次使用
'''
进销存系统登录自动化业务：
1）判断当前目录下jxc-cookies.txt文件是否存在
   如果存在表示已登录，从中读取cookies信息并添加到测试浏览器中
   如果不存在，表示未登录，执行登录测试，登录成功之后，保存cookie
2）登录之后，获取cookie信息并写入到文件中
3）jxc-cookies.txt文件的格式：
   {'name':'xxxx','value':'xxx','domain':''xxxx','path','xxxx'}

'''

import  os #和操作系统交互
import time

from selenium.webdriver import  Chrome
chrome=Chrome()
chrome.get('http://39.101.167.251:81/jinxiaocun/index.asp')
if os.path.exists('jxc-cookies.txt'):
    with open('jxc-cookies.txt', encoding='utf-8') as f:
        for line in f:
            print(line)
            #cookie_dict=eval(line)
            chrome.add_cookie(dict(line))
    time.sleep(2)
   #chrome.refresh()#刷新浏览器
    chrome.get('http://39.101.167.251:81/jinxiaocun/main.asp')
else:
    #之前未登录过。第一次使用
    chrome.find_element_by_css_selector('[name="username"]').send_keys('admin')
    chrome.find_element_by_css_selector('[name="pwd"]').send_keys('admin')
    chrome.find_element_by_css_selector('[name="enter"]').click()

    time.sleep(2)
    #验证是否登录成功
    #当前浏览器的网址current_url，是否是登录成功的网址
    #assert 断言，如果断言失败则抛出AssertionError异常
    assert chrome.current_url=='http://39.101.167.251:81/jinxiaocun/main.asp'

    #获取cookie，并存储
    cookies=chrome.get_cookies()
    print(cookies)
    with open('jxc-cookies.txt','w',encoding='utf-8') as f:
        for cookie_dict in cookies:
            #将dict转成str，再写入文件中
            f.write(str(cookie_dict)+'\n')













