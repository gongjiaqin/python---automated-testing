import time

from selenium.webdriver import ActionChains

from day03.d2 import chrome

left_frame=chrome.find_element_by_name('left')
chrome.switch_to.frame(left_frame)
time.sleep(1)
#点击产品入库
chrome.find_element_by_xpath('//*[@id="g_1"]//tr[1]').click()

#已有产品入库相关操作-right
chrome.switch_to.default_content()
chrome.switch_to.frame(chrome.find_element_by_name('right'))
time.sleep(2)
chrome.find_element_by_name('huohao1').click()

#弹出新的窗口
print(chrome.window_handles)#显示窗口名称列表
#切入到第二个窗口
chrome.switch_to.window(chrome.window_handles[1])
#在第二个窗口中，双击第一个产品
tr1=chrome.find_element_by_css_selector('.toptable tr:nth-child(2)')
actions=ActionChains(chrome)
actions.move_to_element(tr1).double_click().perform()

chrome.switch_to.window(chrome.window_handles[0])
chrome.switch_to.frame(chrome.find_element_by_name('right'))
time.sleep(1)

#输入产品的单价

price=chrome.find_element_by_name('price21')
price.clear()
price.send_keys('50')
time.sleep(1)
#输入数量
shulian=chrome.find_element_by_name('shulian1')
shulian.clear()
shulian.send_keys('10')
time.sleep(1)
#点选仓库
ku=chrome.find_element_by_xpath('//*[@name="ku1"]/option[2]').click()
time.sleep(1)
#点选供应商
gys=chrome.find_element_by_xpath('//*[@name="id_gys"]/option[2]').click()
time.sleep(2)
#输入日期
chrome.execute_script('document.getElementsByName("selldate")[0].value="2022/10/5"')
#
time.sleep(1)
login=chrome.find_element_by_xpath('//*[@name="id_login"]/option[4]').click()
bz=chrome.find_element_by_xpath('//*[@name="beizhu"]').send_keys('冷冻')
time.sleep(1)
chrome.find_element_by_xpath('//input[@type="submit"]').click()
time.sleep(2)
chrome.quit()