import time

from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.select import Select

from day03.d2 import chrome
time.sleep(2)

#d2脚本执行完成后，才会执行d3的脚本程序
print('---day03/d3----')
#目标：下拉框操作
#添加会员所在的父标签 id="g_5"
#会员管理 所在的表格 class="menuall
#会员管理菜单在frame窗口中，它的name="left"
left_frame=chrome.find_element_by_name('left')
chrome.switch_to_frame(left_frame)#进入到left窗口中
#定位会员管理元素
usermgr=chrome.find_element_by_xpath('//*[@class="menuall"]/tbody/tr[11]')
usermgr.click()
time.sleep(2)
#获取它的下一个兄弟标签
umgr_next:WebElement=chrome.execute_script('return arguments[0].nextElementSibling',usermgr)
#从umgr_next 中查找 添加会员
#【注意】从当前标签下查找后代标签时，通过xpath路径上，可能需要.//
umgr_next.find_element_by_xpath('.//tr[1]').click()
time.sleep(1)
#从left退出到默认页面上
chrome.switch_to_default_content()
#再进入到right的frame窗口
right_frame=chrome.find_element_by_name('right')
chrome.switch_to_frame(right_frame)
time.sleep(1)
#查找组的下拉框元素
zu=chrome.find_element_by_name('id_zu')
#将下拉框的元素转化为Select实例
zu_select=Select(zu)
#从下拉框中选择数据,几种方式：
'''
1)index 索引
2)value 属性
3)visible_text 标签文本
'''
#zu_select.select_by_index(2)
#zu_select.select_by_value('23')   #<option value='23'>
zu_select.select_by_visible_text('viip')






