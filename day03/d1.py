#目标：移动鼠标到指定的位置、左/右击
import time

from selenium.webdriver import Chrome, ActionChains
from selenium.webdriver.common.keys import Keys

chrome=Chrome()
chrome.get('https://www/baidu/com')
kw=chrome.find_element_by_id('kw')
#方法需要接收driver的参数，传入浏览器实例对象即可
actions=ActionChains(chrome)
#编排动作：将鼠标移动到搜索框——>右击-->执行
actions.move_to_element(kw)
actions.context_click()
actions.perform()

time.sleep(2)

#滑动效果的动作链
#actions.key_down(Keys.ENTER)
# actions.move_by_offset(0,100)#移动偏移量
# actions.key_up()
# actions.perform()













