import time
from selenium.webdriver.remote.webelement import WebElement
from day03.d2 import chrome
time.sleep(2)

#d2脚本执行完成后，才会执行d3的脚本程序
print('---day03/d3----')
#目标：入库管理-》产品入库-》产品图片-》选择文件

left_frame=chrome.find_element_by_name('left')
chrome.switch_to_frame(left_frame)#进入到left窗口中
#定位会员管理元素
# usermgr=chrome.find_element_by_xpath('//*[@class="menuall"]/tbody/tr[3]')
# usermgr.click()
# time.sleep(2)
#umgr_next:WebElement=chrome.execute_script('return arguments[0].nextElementSibling',usermgr)
#umgr_next.find_element_by_xpath('.//tr[1]').click()

chrome.find_element_by_xpath('//*[@id="g_1"]//tr[1]').click()
time.sleep(1)
chrome.switch_to_default_content()
#再进入到right的frame窗口
right_frame=chrome.find_element_by_name('right')
chrome.switch_to.frame(right_frame)
time.sleep(1)
# js=f'document.documentElement.scrollTop=10000'#当前页面.当前页面的对象.对象的滚动方法
##滚动right frame到底部
#frames[2]是right的frame标签
#.scrollBy(x, y)滚动x水平，y垂直方向页面位置

chrome.execute_script('frames[2],scrollBy(0,1000)')
time.sleep(2)
iframe=chrome.find_element_by_tag_name('iframe')
chrome.switch_to.frame(iframe)
time.sleep(2)
#file1不让点击
#直接设置文件路径（上传文件路径）
#chrome.find_element_by_css_selector('[name="file1"]').click()
path=r'C:\Users\nz\Pictures\BingWallpaper.jpg'
file1=chrome.find_element_by_css_selector('[name="file1"]')
file1.send_keys(path)
#点击上传按钮
time.sleep(1)
chrome.find_element_by_name('submit').click()

#输入产品名称-right的frame
#chrome.switch_to_default_content()  #从 iframe 返回到 right 的frame
chrome.switch_to_default_content()   #返回到最顶层页面,不在页面内，位于页面之上
right_frame=chrome.find_element_by_name('right')
chrome.switch_to.frame(right_frame)#之前查出的right_frame直接切入出现 no attach(失效/失联）
time.sleep(2)
chrome.find_element_by_name('title').send_keys('中秋月饼')







