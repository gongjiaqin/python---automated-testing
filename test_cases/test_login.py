import unittest

from ddt import ddt, data, unpack

from page_objects.login_page import LoginPage
from test_cases import chrome
from test_datas import login_data

@ddt()
class TestLoginPage(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.page = LoginPage(chrome)

    def setUp(self) -> None:
        self.page.open()

    def tearDown(self) -> None:
        chrome.delete_all_cookies()

    @classmethod
    def tearDownClass(cls) -> None:
        cls.page = None


    @data(*login_data.datas())
    @unpack
    def test_login(self, name, pwd, expect_type):
        self.page.input_name(name)
        self.page.input_pwd(pwd)
        self.page.login(expect_type)

        # 异常截图