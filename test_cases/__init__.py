'''
【非常重要】
每一个包下的初始化模块，
无论包导入多少次，初始化模块只会执行一次
'''

from utils.chrome import ChromeWrapper


chrome = ChromeWrapper()