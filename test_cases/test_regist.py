import unittest
import os
import time

from ddt import ddt, data, unpack

from page_objects.regist_page import RegistPage
from test_cases import chrome
from test_datas import regist_data
from common import RESULT_IMG_DIR
from utils.xlsx import write_data


@ddt()
class TestRegistPage(unittest.TestCase):
    # 不创建浏览器对象， 所有测试用例使用同一个浏览器
    # 浏览器对象什么时候创建？  入口创建
    # 从入口导入浏览器对象
    @classmethod
    def setUpClass(cls) -> None:
        cls.page = RegistPage(chrome)

    def setUp(self) -> None:
        self.page.open() # 打开注册页面

    def tearDown(self) -> None:
        chrome.delete_all_cookies()

    @classmethod
    def tearDownClass(cls) -> None:
        cls.page = None  # 引用记数为0时，GC(垃圾回收)执行时会释放引用占用的内存空间

    @data(*regist_data.datas2())
    @unpack
    def test_regist(self, name, email, pwd, repwd, expect):
        self.page.input_username(name)
        self.page.input_email(email)
        self.page.input_password(pwd)
        self.page.input_repassword(repwd)

        try:
            self.page.regist(expect)
            # 可能报异常，异常发生时即代表期待元素没有出现，需要保留现场（截图）
        except Exception as e:
            # 截图
            # time.localtime() 获取本地的当前时间元组对象
            # time.strftime(format, t) 将t时间元组转化format格式的字符串
            # time.strptime(str, format) 将str字符串按format格式转化为时间元组
            cur_time = time.strftime('%Y%m%d%H%M%S', time.localtime())
            path = os.path.join(RESULT_IMG_DIR, f'注册失败-{cur_time}.png')
            chrome.save_png(path)

            # 写入到excel错误的日志文件中
            write_data(f'regist_error_{cur_time}.xlsx',
                       [name, email, pwd, repwd, str(e), path])
            raise e  # 继承抛出异常
