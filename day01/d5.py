from selenium.webdriver import Chrome
import time
#1.创建浏览器
from selenium.webdriver.common.by import By

chrome=Chrome()
#2.打开网站
chrome.get('http://39.101.167.251:81/jinxiaocun/index.asp')
#3.查找元素：3个input标签
inputs=chrome.find_elements_by_tag_name('input')#列表
#3.1向用户名输入框输入admin账号
#3.2向口令输入框输入admin密码
#3.3点击登录按钮
inputs[0].send_keys('admin')
inputs[1].send_keys('admin')
inputs[2].click()
#等待进入主页
time.sleep(1)
#截屏存证
chrome.save_screenshot('jxc-main.png')
#4.查找登出的标签
#1)存在多个标签的class='blue',所以根据链接文本查找
#又因为，安全退出在frame标签里面，所以需要解决：如下：根据name查找，切入到窗口
topFrame=chrome.find_element_by_name('topFrame')
chrome.switch_to.frame(topFrame)
#2)再根据文本局部查找,点击
chrome.find_element_by_partial_link_text('安全退出').click()
#chrome.find_element(By.PARTIAL_LINK_TEXT,'安全退出').click()
time.sleep(1)
#3)对于弹出对话框，确认对话框:
#accept()确认，dismiss()取消
chrome.switch_to.alert.accept()
chrome.save_screenshot('jxc-login.png')
#5.退出浏览器
chrome.quit()





















