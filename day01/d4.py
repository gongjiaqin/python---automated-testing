# 1.查找搜索框和搜索按钮
# 2.向输入框录入数据
# 3.点击元素：点击搜索按钮
# 4.关闭、截图、最大化窗口、获取源码、获取当前网页标签的标题、退出浏览器
from selenium.webdriver.remote.webelement import WebElement
from day01.d2 import chrome
import time
chrome.get('http://www.baidu.com')
# 打开网站可能需要等一会
time.sleep(2)
# 获取搜索框元素
# find_element()默认的By方式是ID
input: WebElement = chrome.find_element(value='kw')
# 获取搜索按钮
search_btn :WebElement= chrome.find_element(value="su")
print(input, search_btn)
search_btn.screenshot('search.png')#截按钮：查到按钮后就截图，否则，跳转页面后就找不到这个按钮


#2—录入搜索的内容
# 1）一次性输入
#input.send_keys('中秋节')
# 2）一个一个输入一个字
for word in '中秋节':
    input.send_keys(word)
    time.sleep(0.5)


# 3-点击元素：点击搜索按钮
search_btn.click()


# 4-最大化窗口、截图、关闭标签、获取源码、获取当前网页标签的标题、退出浏览器
chrome.maximize_window()#最大化窗口
time.sleep(2)
#search_btn.screenshot('search.png')--报错
#报错：element not attached to the page element
#原因:search_btn元素不是从当前网页中查找定位的
chrome.save_screenshot('baidu.png')#截全屏


chrome.quit()#退出浏览器










