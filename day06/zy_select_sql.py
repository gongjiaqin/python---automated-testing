from day06.con_db import conn
#实现数据库的查找select和insert插入sql语句
def query(table, *args, **wheres):
    sql='select %s from %s '
    goal=','.join({i for i in args})
    if wheres:
        sql+='where %s'
        where='and '.join(f"{k}='{v}'"if type(v)==str else f"{k}={v}" for k,v in wheres.items())
        sql=sql%(goal,table,where)
    else:
        sql=sql%(goal,table)
    print(sql)
    cursor = conn.cursor()
    cursor.execute(sql)
    rows=cursor.fetchall()
    print(rows)
    conn.commit()
    cursor.close()

def insert(table,**kwargs):

    sql='insert into %s(%s) values(%s)'
    feilds=','.join(kwargs)
    values=','.join(f"'{v}'" if type(v)==str else str(v) for v in kwargs.values())
    sql=sql%(table,feilds,values)
    print(sql)
    cursor = conn.cursor()
    cursor.execute(sql)
    conn.commit()
    cursor.close()

if __name__ == '__main__':
    #insert('stu',stuid='220502',stuname='disen',stuage=22,stuaddr='北京')
    query('stu','*',stuage=22)