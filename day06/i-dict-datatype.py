g1=(i for i in [1,2,3])#生成器推导式
print(type(g1))
d1={#字典的key支持可以hash的数据，即不可变的数据类型
    # int，float，bool,None,bytes,
    # 字符串str、元组tuple、函数,自定义类的对象
    # 生成器：创建方式：1）推导式 2）range
    1:1,
    1.1:'good',
    True:1,
    None:2,
    b'123':90,
    (1,):2,
    range(1,5):9, #步长生成器
    g1:10,
    print:0,
    object:0
}
print(d1)
print({v:k for k,v in d1.items()})