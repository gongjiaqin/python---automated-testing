g1=(i for i in range(1,20))#生成器
#生成器迭代方式：
# 1）for in
# 2)next(),while,try-except
#1)

for m in g1:
    print(m,'',end='')
#2)
n=next(g1)#从生成器中提取一个元素
while True:
    print(n,'',end='')
    try:
        n=next(g1)#如果没有元素时，报StopIteration(停止迭代)异常
    except:
        break

#判断输入的数是不是素数
'''
n=int(input('输入一个数：'))
for m in range(2,n):
    if n%m==0:
        print(n,'不是质数')
        break
else:
    print(n,'是质数')

#输出4个100以内的质数
import random
s=set()
while len(s)<4:
    n = random.randint(1, 100)
    for m in range(2,n):
        if n%m==0:
            break
    else:
        s.add(n)
print(s)
'''
