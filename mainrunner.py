import unittest

from BeautifulReport import BeautifulReport
from test_cases import chrome

if __name__ == '__main__':
    suite = unittest.TestSuite()

    # discover(dir) 从dir目录中获取以test_开头的所有模块中所有测试方法
    # defaultTestLoader 是TestLoader类的实例
    suite.addTests(unittest.defaultTestLoader.discover('./test_cases'))

    bf = BeautifulReport(suite)
    bf.report('电商之用户注册与登录', './test_results/20220913-2.html')

    chrome.quit()